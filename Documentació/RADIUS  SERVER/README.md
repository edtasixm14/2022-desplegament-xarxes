
## RADIUS Server:
### Què és un RADIUS Server?
La nomenclatura "RADIUS" fa referència a Remote Authentication Dial-In User Server, és un protocol d'autenticació i autorització per a aplicacions d'accés a la xarxa. Utilitza el port 1812 per a l'autenticació i autorització d'usuaris.
<a href="https://imgbb.com/"><img src="https://i.ibb.co/jymKDg7/radius.png" alt="radius" border="0"></a>   

  
L'ús més freqüent es per a accedir a xarxes wifi, també es pot fer servir per a accedir a VPN i a altres aplicatius de xarxa.

### Com implementar RADIUS a la teva xarxa:

#### Instal·Lació
RADIUS es pot instal·lar de forma molt senzilla, hem de seguir els passos següents amb root:

> apt update  
> apt install freeradius  
> systemctl start freeradius  
> systemctl enable freeradius

Si tenim firewall ens assegurem que no ens bloquegi el port
> ufw allow 1812

#### Configuració i integració amb LDAP
Un dels avantatges de RADIUS és que té moltes opcions per configurar-lo i gracies a la seva modularitat, amb l'instal·lació d'alguns mòduls es pot integrar amb LDAP, Active Directory, MySQL, PostgreSQL i fins i tot Oracle i altres BBDD.

El fitxer de configuració per defecte es troba en el directori "/etc/freeradius/3.0/radiusd.conf"


>vim /etc/freeradius/3.0/radiusd.conf

En aquest fitxer mateix habilitem la línia que activa el mòdul LDAP:


>Authorize {        
           #  
        #  The ldap module will set Auth-Type to LDAP if it has not already been set  
        ldap

definim al fitxer "/etc/freeradius/3.0/clients.conf" la xarxa o xarxes on permetrem autenticar els clients.

>vim /etc/freeradius/3.0/clients.conf

Li definim per exemple:

>client 192.168.0.0/24 {
    secret          = clau-acces-xarxa
    shortname       = Nombre de la xarxa privada
}

Per fer que RADIUS estigui actiu en el següent inici del sistema i en tots els nivells d'arrencada:
>chkconfig radiusd on


#### Configuració PA Wifi
Si volem que RADIUS autentiqui les connexions wifi hem de connectar a la xarxa un router o punt d'accés compatible amb la versió enterprise de wpa2 o wpa3. 

<a href="https://imgbb.com/"><img src="https://i.ibb.co/qCwTWFQ/1610508898-469-Como-configurar-WPA2-Enterprise-en-su-red.png" alt="1610508898-469-Como-configurar-WPA2-Enterprise-en-su-red" border="0"></a>  

A la Configuració de seguretat de la wifi en el mode wpa2/3-enterprise hem d'introduir la IP del servidor RADIUS, el port, i el secret que hem definit.
Fet això, ens podrem autenticar amb els usuaris LDAP per wifi.

#### Verificació del correcte funcionament

Per verificar el correcte funcionament podem provar a connectar-nos a la wifi des d'un dispositiu i comprovar que ens podem connectar amb les nostres credencials o fer la següent comanda per fer una request al servidor.
>radtest usuari "passwd usuari ldap" **< ip servidor >** 2 **< secret compartit >**

La resposta hauria de ser aixi:
>Sending Access-Request of id 191 to 192.168.0.1:1812  
         User-Name = "usuari-ldap"  
         User-Password = "clau-acces-ldap"  
         NAS-IP-Address = nom-servidor  
         NAS-Port = 2  
rad_recv: Access-Accept packet from host 192.168.0.1:1812, id=191, length=20