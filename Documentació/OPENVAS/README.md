
## OpenVAS:
### Què és OpenVAS?
Openvas es el nom pel que es coneix l'actual Greenbone Vulnerability Management (GVM). És un escàner de vulnerabilitats que pot detectar problemes de diferents tipus, tant de baix risc per a usuaris com vulnerabilitats més greus en equips i dispositius de xarxa. Compta amb més de 50.000 "Network vulnerability tests" (NVTs) i dades de vulnerabilitats conegudes i alimentades diàriament per l'empresa propietària i per part de la comunitat i els seus experts, a través d'una interfície gràfica web podem fer els testos i gestionar l'eina.

### Instal·lació GVM 21.04 a Debian 11:

#### Prerequisits mínims recomanats:
* 4 GB de RAM
* 4 nuclis de CPU
* 8 GB d'espai en disc

#### Actualització del sistema:
Per començar hem d'actualitzar el sistema.

> apt update; apt upgrade

#### Creem l'usuari GVM al sistema.

>useradd -r -d /opt/gvm -c "GVM User" -s /bin/bash gvm

Creem el directori anteriorment especificat definint com a usuari i grup propietari "gvm".

>mkdir /opt/gvm && chown gvm: /opt/gvm

#### Instal·lem les eines requerides 
Instal·lem eines i dependències que es requereixen per instal·lar GVM.

>apt install gcc g++ make bison flex libksba-dev curl redis libpcap-dev cmake git pkg-config libglib2.0-dev libgpgme-dev nmap libgnutls28-dev uuid-dev libssh-gcrypt-dev libldap2-dev gnutls-bin libmicrohttpd-dev libhiredis-dev zlib1g-dev libxml2-dev libnet-dev libradcli-dev clang-format libldap2-dev doxygen gcc-mingw-w64 xml-twig-tools libical-dev perl-base heimdal-dev libpopt-dev libunistring-dev graphviz libsnmp-dev python3-setuptools python3-paramiko python3-lxml python3-defusedxml python3-dev gettext python3-polib xmltoman python3-pip texlive-fonts-recommended texlive-latex-extra --no-install-recommends xsltproc sudo vim rsync -y

##### Instal·lem Yarn
Instal·lem Yarn JavaScript package manager  
 
>curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | sudo tee /usr/share/keyrings/yarnkey.gpg >/dev/null  
 
>echo "deb [signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

>apt update;apt upgrade

>apt install yarn -y

##### Instal·lem PostgreSQL

>echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list
curl -sL https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo tee /etc/apt/trusted.gpg.d/pgdg.asc >/dev/null
apt update
apt install postgresql-11 postgresql-contrib-11 postgresql-server-dev-11 -y

##### Configurem usuari i base de dades postgreSQL

>sudo -Hiu postgres
createuser gvm
createdb -O gvm gvmd  

Assignem rols.  
>psql gvmd
create role dba with superuser noinherit;
grant dba to gvm;
\q
exit  

Reiniciem postgreSQL.
>systemctl restart postgresql  
>systemctl enable postgresql

Autoritzem l'usuari gvm a instal·lar amb permisos de sudo.
>echo "gvm ALL = NOPASSWD: $(which make) install" > /etc/sudoers.d/gvm

#### Instal·lem GVM 21.04

Entrem a l'usuari creat anteriorment.

>su - gvm

Creem un directori on descarregar els fitxers d'instal·lació.
>mkdir gvm-source

##### Descarreguem els fitxers d'instal·lació

>cd gvm-source

>git clone -b stable --single-branch https://github.com/greenbone/gvm-libs.git;  
>git clone -b main --single-branch https://github.com/greenbone/openvas-smb.git;  
>git clone -b stable --single-branch https://github.com/greenbone/openvas.git;  
>git clone -b stable --single-branch https://github.com/greenbone/ospd.git;  
>git clone -b stable --single-branch https://github.com/greenbone/ospd-openvas.git;  
>git clone -b stable --single-branch https://github.com/greenbone/gvmd.git;  
>git clone -b stable --single-branch https://github.com/greenbone/gsa.git;  
>git clone -b stable --single-branch https://github.com/greenbone/gsad.git


Hem de ser al directori "/opt/gvm/gvm-source" i si fem un ls -1 ens han de sortir les carpetes següents:

* gsa
* gvmd
* gvm-libs
* openvas
* openvas-smb
* ospd
* openvas-smb

##### Comencem amb la instal·lació

Des del directori "/opt/gvm/gvm-source" instal·lem les llibreries de GVM:

>cd gvm-libs  
mkdir build && cd build  
cmake ..  
make  
sudo make install  

Instal·lem l'escàner compatible amb windows d'openVAS (openvas-smb):

>cd ../../openvas-smb/  
mkdir build && cd build  
cmake ..  
make  
sudo make install  

Instal·lem l'escàner openVAS:

>cd ../../openvas  
[ -d build ] || mkdir build && cd   build  
cmake ..  
make  
sudo make install  

##### Configurem openVAS

La informació d'escaneig de host està guardada temporalment al servidor Redis.  

Tornem a root

>exit  

Creem la memòria cau a les biblioteques instal·lades

>ldconfig

La configuració del servidor redis es troba a "/etc/redis/redis.conf".


A continuació, copiem el fitxer de configuració Redis de l'escàner OpenVAS des del directori font d'OpenVAS, redis-openvas.conf, al directori de configuració de Redis
> cp /opt/gvm/gvm-source/openvas/config/redis-openvas.conf /etc/redis/

Canviem el propietari:
>chown redis:redis /etc/redis/redis-openvas.conf

Actualitzem el directori al socket Redis Unix a /etc/openvas/openvas.conf mitjançant el paràmetre db_address.

Per obtenir el directori al socket fem la comanda:
>grep unixsocket /etc/redis/redis-openvas.conf

Que ens donarà una sortida com, per exemple:
>unixsocket **/run/redis-openvas/redis.sock**
unixsocketperm 770

Un cop obtenim el directori fem la comanda:
>echo "**db_address = /run/redis-openvas/redis.sock**" > /etc/openvas/openvas.conf

Afegim l'usuari gvm al grup redis.

>usermod -aG redis gvm

##### Actualitzem els Network Vulnerability Tests (NVTs)
Ens assegurem que l'usuari gvm pot escriure al directori de les llibreries d'OpenVAS.
>chown -R gvm: /var/lib/openvas/

També li donem permisos per a executar OpenVAS amb sudo  
>echo "gvm ALL = NOPASSWD: $(which openvas)" >> /etc/sudoers.d/gvm

Procedim a actualitzar els NVTs amb l'usuari gvm

>su - gvm  
>greenbone-nvt-sync --rsync

Després d'actualitzar la informació pugem els tests al servidor redis.

>sudo openvas --update-vt-info

##### Instal·lem GVM


>cd gvm-source/gvmd  
mkdir build && cd build  
cmake ..  
make  
sudo make install

##### Instal·lem "Greenbone Security Assistant"
És la interfície web gràfica que farem servir.
>cd ../../gsa  
rm -rf build  
yarn  
yarn build  

El servidor web de greenbone.
>cd ../gsad
mkdir build && cd build
cmake ..
make
sudo make install

Després copiem les configuracions de la interfície web

>[[ -d /usr/local/share/gvm/gsad/web ]] || mkdir -p /usr/local/share/gvm/gsad/web

.
>chown -R gvm: /usr/local/share/gvm/gsad/web

.
>cp -rp /opt/gvm/gvm-source/gsa/build/* /usr/local/share/gvm/gsad/web

##### Mantenir les dades actualitzades completament

Tornem a root. 

>exit

.
>chown -R gvm: /var/lib/gvm/

Per mantenir actualitzades les dades hauríem d'executar les següents ordres regularment:

>sudo -u gvm greenbone-feed-sync --type GVMD_DATA

.
>sudo -u gvm greenbone-feed-sync --type SCAP

.
>sudo -u gvm greenbone-feed-sync --type CERT

##### Generem els certificats de GVM

sudo -u gvm gvm-manage-certs -a

##### Instal·lem OSP (Open Scanner Protocol)
Serveix per a tenir els diferents escàners de seguretat i el control centralitzats amb Greenbone.

>su - gvm  
pip3 install wheel  
pip3 install python-gvm gvm-tools
cd /opt/gvm/gvm-source/ospd  
python3 -m pip install .  
cd /opt/gvm/gvm-source/ospd-openvas  
python3 -m pip install .  

##### Creem la unitat de systemd per OpenVAS OSPD

>cat > /etc/systemd/system/ospd-openvas.service << 'EOL'  
[Unit]
Description=OSPd Wrapper for the OpenVAS Scanner (ospd-openvas)
After=network.target networking.service redis-server@openvas.service postgresql.service
Wants=redis-server@openvas.service
ConditionKernelCommandLine=!recovery

>[Service]
ExecStartPre=-rm -rf /var/run/gvm/ospd-openvas.pid /var/run/gvm/ospd-openvas.sock
Type=simple
User=gvm
Group=gvm
RuntimeDirectory=gvm
Environment=PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/gvm/bin:/opt/gvm/sbin:/opt/gvm/.local/bin
ExecStart=/opt/gvm/.local/bin/ospd-openvas \
--pid-file /var/run/gvm/ospd-openvas.pid \
--log-file /var/log/gvm/ospd-openvas.log \
--lock-file-dir /var/run/gvm -u /var/run/gvm/ospd-openvas.sock
RemainAfterExit=yes

>[Install]
WantedBy=multi-user.target
EOL

Canviem el propietari i grup de les carpetes a gvm  
>[[ -d /var/run/gvm ]] || mkdir /var/run/gvm
>
chown -R gvm: /var/run/gvm /var/log/gvm

Si volem comprovar que s'ha creat sense cap problema:

>systemctl daemon-reload
>systemctl enable --now ospd-openvas
>systemctl status ospd-openvas.service

##### Modifiquem l'unitat de systemctl de GVM

>cp /lib/systemd/system/gvmd.service{,.bak}

.
>cat > /lib/systemd/system/gvmd.service << 'EOL'
[Unit]
Description=Greenbone Vulnerability Manager daemon (gvmd)
After=network.target networking.service postgresql.service ospd-openvas.service
Wants=postgresql.service ospd-openvas.service
Documentation=man:gvmd(8)
ConditionKernelCommandLine=!recovery

>[Service]
Type=forking
User=gvm
Group=gvm
RuntimeDirectory=gvmd
Environment=PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/gvm/bin:/opt/gvm/sbin:/opt/gvm/.local/bin
ExecStart=/usr/local/sbin/gvmd --osp-vt-update=/var/run/gvm/ospd-openvas.sock
Restart=always
TimeoutStopSec=10

>[Install]
WantedBy=multi-user.target
EOL


Si volem comprovar que s'ha creat sense cap problema:

>systemctl daemon-reload
>systemctl enable --now gvmd
>systemctl status gvmd

##### Creem la unitat de systemd per GSA

>cp /lib/systemd/system/gsad.service{,.bak}

.

>cat > /lib/systemd/system/gsad.service << 'EOL'
[Unit]
Description=Greenbone Security Assistant daemon (gsad)
Documentation=man:gsad(8) https://www.greenbone.net
After=network.target gvmd.service
Wants=gvmd.service  
[Service]
Type=simple
User=gvm
Group=gvm
RuntimeDirectory=gsad
PIDFile=/var/run/gsad/gsad.pid
Environment=PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/gvm/bin:/opt/gvm/sbin:/opt/gvm/.local/bin
ExecStart=/usr/bin/sudo /usr/local/sbin/gsad -k /var/lib/gvm/private/CA/clientkey.pem -c /var/lib/gvm/CA/clientcert.pem
RemainAfterExit=yes  
[Install]
WantedBy=multi-user.target
EOL


Habilitem l'usuari GVM a executar gsad amb sudo.

>echo "gvm ALL = NOPASSWD: $(which gsad)" >> /etc/sudoers.d/gvm

Si volem comprovar que s'ha creat sense cap problema:

>systemctl daemon-reload
>systemctl enable --now gsad
>systemctl status gsad


#####  Creem i registrem el nostre escaner de OpenVAS

>sudo -u gvm gvmd --create-scanner="Kifarunix-demo OpenVAS Scanner" \
--scanner-type="OpenVAS" --scanner-host=/var/run/gvm/ospd-openvas.sock

Per a verificar que s'ha creat correctament

>sudo -u gvm gvmd --get-scanners  

>sudo -u gvm gvmd --verify-scanner="codi de l'escaner"

##### Usuari administrador de GVM

>sudo -u gvm gvmd --create-user admin

Si vols crear un altre usuari:
>sudo -u gvm gvmd --create-user **USERNAME** --password=**PASSWORD**

Per a modificar una contrasenya:
>sudo -u gvm gvmd --user=<**USERNAME**> --new-password=<**PASSWORD**>

##### Definim el propietari

>sudo -u gvm gvmd --get-users --verbose

En la següent comanda posem el valor de la comanda anterior.
>sudo -u gvm gvmd --modify-setting 78eceaec-3385-11ea-b237-28d24461215b --value **bb3bd8a6-6b77-464f-9f9b-1afe4835be15**

##### Accedir a la interfície:

Ara que ja ho tenim tot llest ens assegurem que podem accedir a la interfície posant l'adreça del servidor a un navegador extern o accedint a localhost si ho fem des del nostre. Si hem realitzat la configuració de DDNS i DMZ o NAT ja hauríem de poder accedir des de xarxes externes.

Si tenim firewall hauríem de fer la següent comanda al servidor:
>ufw allow 443/tcp

### Utilitzar Openvas:

Primer de tot, hem d'accedir a la interfície tal i com s'ha explicat anteriorment. Allà hi introduïm les credencials de l'usuari que havíem creat.  
<a href="https://ibb.co/TMnh4Yh"><img src="https://i.ibb.co/v4rz3Vz/Screenshot-from-2022-05-16-16-30-25.png" alt="Screenshot-from-2022-05-16-16-30-25" border="0"></a>  

Un cop entrem, a la pagina principal (Dashboard) ens sortiran 4 gràfics, el primer de la gravetat de la seguretat de les tasques realitzades i el segon de l'estat de les tasques realitzades, aquests haurien de sortir en blanc, ja que encara no hem realitzat cap test. El tercer i el quart ens informen de les estadístiques dels NVTs que tenim importats. Si algun d'aquests dos ens surt en blanc és que no hem importat bé els tests i els hauriem de repetir.  

<a href="https://ibb.co/QpRPcsz"><img src="https://i.ibb.co/RYm3SLZ/Screenshot-from-2022-05-16-16-34-22.png" alt="Screenshot-from-2022-05-16-16-34-22" border="0"></a>

#### Definir els dispositius per analitzar

EL primer que hauríem de configurar són els dispositius que volem atacar, anant a la pestanya "Configuration" a l'apartat de "Targets". Allà podem definir varies IP de forma individual o en rangs per escanejar i descobrir vulnerabilitats dels dispositius que desitgem. Per fer-ho hem de clicar al cuadrat amb una estrella al marge i afegir l'objectiu on podem definir els ports que volem escanejar d'entre altres coses.  

<a href="https://imgbb.com/"><img src="https://i.ibb.co/gMGBdtF/Screenshot-from-2022-05-16-16-43-48.png" alt="Screenshot-from-2022-05-16-16-43-48" border="0"></a>  

En el meu cas he preferit definir un objectiu per cada IP i màquina de forma individual.  

<a href="https://ibb.co/HHTG93N"><img src="https://i.ibb.co/Ny2WGb1/Screenshot-from-2022-05-16-16-37-34.png" alt="Screenshot-from-2022-05-16-16-37-34" border="0"></a>

#### Escanejar una IP o rang d'IPs

Hem d'anar a l'apartat "Scans" a la pestanya "Tasks" allà clicant un cuadradet amb una estrella a dalt a l'esquerra podem definir una tasca d'escaneig per posar en marxa posteriorment.  

<a href="https://imgbb.com/"><img src="https://i.ibb.co/pR7mYhv/Screenshot-from-2022-05-16-16-49-14.png" alt="Screenshot-from-2022-05-16-16-49-14" border="0"></a>  

Definim el "Target" configurat anteriorment per atacar i escollim l'escàner que hem creat en el moment de l'instal·lació.  

<a href="https://imgbb.com/"><img src="https://i.ibb.co/RD0j5W1/Screenshot-from-2022-05-16-16-51-51.png" alt="Screenshot-from-2022-05-16-16-51-51" border="0"></a>

