
## DDNS:
### Què és DDNS?
La nomenclatura “DDNS" o "DynDNS" fa referència a Dynamic Domain Name System (sistema dinàmic de noms de domini), serveix per a assignar un nom de domini a una xarxa domestica (o no) on la IP pública pot canviar. Cal registrar-se en un servei DDNS amb un nom que estigui lliure com per exemple(exemple.albert.com). Gràcies a ell la teva xarxa sempre estarà disponible encara que no coneguis la seva adreça IP publica 	 actual.  
També es molt útil per no haver de consultar les IP publiques dinàmiques de maquines en el cloud com podria ser una instancia AWS.

### Com implementar DDNS a la teva xarxa:
Hi ha diversos proveïdors de DDNS, nosaltres per exemple farem servir "No-IP", aquest ens dona l'opció de registrar un domini gratuït sempre i quan el nom acabi en "ddns.net". El primer que hem de fer és crear-nos un compte en el proveïdor que ens interessi i registrar el nom de domini que desitgem. Com per exemple: 

<a href="https://ibb.co/QfVNvCW"><img src="https://i.ibb.co/c19FhCW/Screenshot-from-2022-05-14-13-19-59.png" alt="Screenshot-from-2022-05-14-13-19-59" border="0"></a>

Un cop enregistrat el domini, hem de configurar el Router de casa nostra perquè vagi informant de la IP publica que té en tot moment cada cop que hi hagi un canvi. Per això, hem d'entrar al portal de configuració del router i buscar l'apartat de DDNS o DynDNS i introduir les credencials del proveïdor i el nom de host.

<a href="https://ibb.co/smsHMnZ"><img src="https://i.ibb.co/2ncNfxT/Screenshot-from-2022-05-14-16-55-48.png" alt="Screenshot-from-2022-05-14-16-55-48" border="0"></a>

Fet això, en accedir o fer un ping al domini ja ens ha de dirigir a la IP pública de la nostra xarxa.

<a href="https://ibb.co/zfxS2Pg"><img src="https://i.ibb.co/VB9vCMG/Screenshot-from-2022-05-14-17-25-27.png" alt="Screenshot-from-2022-05-14-17-25-27" border="0"></a>

#### Què passa si no vull o puc configurar-ho al router?
Existeix un client que es pot instal·lar a una maquina de la xarxa que faci la mateixa funció, anar informant de la IP publica i si aquesta canvia. S'ha de tenir en compte que ha de ser una maquina que sempre estigui encesa, sinó la IP publica podria canviar i no ens assabentaríem. Si ho volem fer així doncs hem de descarregar el fitxer "noip-duc-linux.tar.gz" que hi ha en aquest mateix repositori i fer les següents comandes:

> (guardar el fitxer descarregat al directori /usr/local/src)
 
> cd /usr/local/src  
tar xzf noip-duc-linux.tar.gz  
cd no-ip-2.1.9  
make  
make install  
/usr/local/bin/noip2 -C

-Se'ns demanarà el nom d'usuari i contrasenya de noip i el nom de host que volem actualitzar  
-Per iniciar el client  

>/usr/local/bin/noip2



### Utilitzar DDNS per a accedir a dispositius de la teva xarxa (DMZ/NAT):
Per començar, primer de tot ens hem d'assegurar que la nostra IP pública no és compartida, ja que degut a l'escassetat d'adreces IP públiques IPv4, algunes de les companyies proveïdores de serveis d'internet fan servir protocols com "CGNAT" o "Dual-Stack Lite" per englobar diversos clients en una sola IP. Cal assegurar-se, doncs, que no som a dins d'una d'aquestes tecnologies, en cas de ser-hi, hem de demanar a la nostra companyia que ens proporcioni una IP pública pròpia, servei que de moment totes les operadores ofereixen de forma gratuïta quan ho sol·licites.

Un cop comprovat això, tenim dues opcions a la configuració del router, la primera, configurar una DMZ per exposar tots els ports d'un ordinador en concret a internet.  
<a href="https://imgbb.com/"><img src="https://i.ibb.co/XVyKHWN/Screenshot-from-2022-05-14-19-36-59.png" alt="Screenshot-from-2022-05-14-19-36-59" border="0"></a>

També es podrien configurar unes regles NAT per exposar uns ports definits d'una o més maquines concretes a internet. Aquesta opció també ens permet exposar un port extern diferent a l'intern, de manera que, per exemple, podríem configurar que per accedir al servidor SSH de forma externa utilitzem el 50000 però en realitat utilitzem el 22.
 
 <a href="https://imgbb.com/"><img src="https://i.ibb.co/1Q8N374/Screenshot-from-2022-05-14-19-38-21.png" alt="Screenshot-from-2022-05-14-19-38-21" border="0"></a>
En aquest cas hem redirigit el port 22 extern al 22 intern per accedir via SSH de forma més fàcil.

<a href="https://ibb.co/Q6S3VGC"><img src="https://i.ibb.co/qRq38wW/Screenshot-from-2022-05-14-19-46-57.png" alt="Screenshot-from-2022-05-14-19-46-57" border="0"></a>

S'ha de tenir en compte que els dos mètodes necessiten que apliquem IP estàtiques a les màquines que volem exposar, ja que les regles s'apliquen a través de les IP de les maquines.