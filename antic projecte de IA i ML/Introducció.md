# INTRODUCCIÓ
# Què son l'aprenentatge automàtic i la intel·ligencia artificial?
### (L'aprenentatge automàtic es coneix més com a machine learning abreviat "ML" i la intel·ligenvia artificial com a "IA")
La intel·ligencia artificial es refereix a l'aplicació de tecniques informatiques que permeten a una computadora adquirir habilitats similars a les d'una inteligencia humana i apendre per si sola. Per apendre fa servir ML, que es basa en una gran quantitat de dades per preveure resultats i trobar algoritmes que es repeteixen. Per tant, per arribar a la IA primer s'ha d'acabar d'implementar el ML a tots els llocs on es pugui implementar.  

# Automatització, aprenentatge automàtic i IA: Que en pot aprofitar un administrador del sistemes?  
## Moltes vegades les tasques d'un administrador de sistemes consisteixen en automatitzar la feina que han de fer. Tot i tenir moltes eines automatitzades, encara ens queda molta feina.  
### Com ens afectarà la IA i l'aprenentatge automàtic?
El primer pas abans d'arribar a la IA, és l'automatització, l'automatització no es algo nou a les TIC, els programes i els scripts gestionen algunes tasques repetitives que fins fa uns anys requerien un equip d'informatics. Eines d'automatització, com Puppet, Chef o Ansible, proporcionen una gestió centralitzada de la configuració del sistema, facilitant molt la feina als administradors del sistema.  
El següent pas és l'aprenentatge automàtic, l'aprenentatge automàtic consisteix en una part de la IA.
L'aprenentatge automatic consisteix en buscar uns patrons en una gran quantitat de registres i informació. Tot i que la majoria d'aquesta informació pot ser inútil, hi ha patrons en aquests registres que podrien indicar problemes, àrees potencials de millora i altres coses. El que en limita l'avenç és que es requeireix d'unes dades molt concretes i filtrades per poguer-ho fer.  
Ara mateix la IA ens queda molt lluny per aplicarla a fer tasques d'administració informàtica, l'aprenentatge automatic és el que ara estan en implementació.  
Ara mateix la IA esta revolucionant el món de la industria, les fabriques optimitzen la producció utilitzant eines de machine learning.  



### L'aprenentatge automàtic que tenim a les mans actualment:  
Avui en dia tenim Machine Learning a molts llocs sense saber-ho, des del movil fins a les infraestructures publiques com les del metro o el tren.  

Començant pel mòvil coneixem tots des de fa anys l'eina del teclat predictiu, que a base de saber què escribim habitualment preveu les paraules correctes. Però hi ha altres funcions més recents que per exmple s'han implementat en les ultimes versions D'iOS, com: les prediccions del calendari, l'app de fotos i les seggerencies de l'assistent de veu siri.  

<img src="https://aatma.es/wp-content/uploads/2021/03/%C2%BFComo-puedo-desactivar-la-sugerencia-de-prediccion-de-teclado.png" width="300px">  <img src="https://www.iphonetricks.org/wp-content/uploads/2018/10/ios-12-siri-suggestion-1200x1200.jpg" width="300px">


A nivell de sistemes operatius d'ordinadors, windows ha començat a implementar alguna nova funció amb IA als widgets i a traves de les hores d'us preveu quan utilitzes segons quines coses.  

<img src="https://www.pchardwarepro.com/wp-content/uploads/2019/06/Automatically-Change-Active-Hours-Windows-10.png" width="500px">

A nivell de Cloud, google esta molt avançada en el camp del big data i machine learning, google fa servir les dades dels seus milions d'usuaris per saber-ne els moviments, els algoritmes de comportament, i sobretot el rastreig per saber els gustos de la gent per poder fer-li publicitat del tema.  

<img src="https://economiasustentable.com/wp-content/uploads/2021/03/Google-maps.jpg" width="500px">

Per ultim, a les infraestructures publiques es començen a veure també molt bones aplicacions per al machine learning, dos exemples en son Renfe i TMB, renfe ha incorporat cameres a les estacions que fan el seguiment del comportament de la gent i aixi preveure actes vandalics o conseguir les estadistiques del passatge dels trens.  

<img src="https://i0.wp.com/gacetinmadrid.com/wp-content/uploads/2022/02/pantallazo.jpg?resize=800%2C445&ssl=1" width="500px">

TMB en canvi ha incorporat la mateixa tecnologia p monitoritzar la ventilació de les estacions de metro de la L1 i els ordinadors dels nous models de tren a traves de les dades d'averies poden preveure quan s'espatllarà alguna cosa com una porta abans que sigui tard.  

Altres funcions que s'han incorporat ultimament al nostre dia a dia es la detecció del contingut de les nostres fotos, això s'ha aplicat a google fotos, la galeria d'apple i alguns android.  

<img src="https://storage.googleapis.com/openimages/web/images/fiftyone.png" width="500px">
